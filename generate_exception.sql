CREATE OR REPLACE FUNCTION generate_dummy_exception()
  RETURNS trigger AS
$BODY$
BEGIN
	
	IF NEW.ini_submission_identifier_str = 'raise_exception' THEN

		INSERT INTO nonexistent_table(C1) VALUES(NULL);
	 
	END IF;

 RETURN NEW;
END;
$BODY$
		
LANGUAGE plpgsql VOLATILE
COST 100;
		
CREATE TRIGGER generate_dummy_exception_trigger
	before update
   ON tbl_invoice_import_ini
   FOR EACH ROW EXECUTE PROCEDURE generate_dummy_exception();